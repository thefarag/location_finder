from euclid import Point2, Circle, LineSegment2
class BeaconSystem:
    def __init__(self, point_location, beacon_locations, strength_to_beacons):
        self.beacon_location_strength_ratio = []
        self.beacon_locations = beacon_locations
        for i in range(3):
            distance = beacon_locations[i].distance(point_location)
            ratio = distance / strength_to_beacons[i]
            self.beacon_location_strength_ratio.append(ratio)
    
    def get_location(self, beacon_strengths):
        circles = []
        points_of_intersection = []
        for i in range(3):
            distance = beacon_strengths[i] * self.beacon_location_strength_ratio[i]
            circles.append(Circle(self.beacon_locations[i], distance))
        for i in range(3):
            intersections = circles[i].intersect(circles[(i+1)%3])
            print(intersections)
            if not intersections:
                continue
            elif isinstance(intersections, Point2):
                intersection = intersections
            elif len(intersections) == 2:
                intersection = LineSegment2(intersections[0], intersections[1])
            elif len(intersections) == 1:
                intersection = intersections[1]
           
            line_intersection = circles[(i+2)%3].intersect(intersection)
            if intersections:
                print(line_intersection)
                xs = line_intersection.p.x + line_intersection.p.x + line_intersection.v.x
                ys = line_intersection.p.y + line_intersection.p.y + line_intersection.v.y
                point = Point2(xs/2, ys/2)
                print(point)
                points_of_intersection.append(point)
        sumx = 0
        sumy = 0
        for point in points_of_intersection:
            sumx += point.x
            sumy += point.y
        return Point2(sumx / len(points_of_intersection), sumy / len(points_of_intersection))

def main():
    
    my_location = Point2(0, 0)
    beacons_location = [Point2(-4, 0), Point2(0, 4), Point2(4, 0)]
    strength_to_beacons = [8, 8, 8]
    beaconSystem = BeaconSystem(my_location, beacons_location, strength_to_beacons)
    while(True):
        beacon_strengths = []
        for i in range(3):
            s = input()
            beacon_strengths.append(float(s))
        location = beaconSystem.get_location(beacon_strengths)
        print("result:")
        print(location)
    

if __name__ == "__main__":
    main()